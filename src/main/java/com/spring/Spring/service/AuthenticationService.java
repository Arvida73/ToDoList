package com.spring.Spring.service;

import com.spring.Spring.dao.UserEntity;
import com.spring.Spring.dto.LoginRequest;
import com.spring.Spring.dto.RegisterRequest;
import com.spring.Spring.mapper.UserMapper;
import com.spring.Spring.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
@Slf4j
public class AuthenticationService {

    private final UserMapper userMapper;
    private final UserRepository userRepository;

    @Transactional
    public void signup(RegisterRequest registerRequest) {
        UserEntity isInDatabase = userRepository.findByUserName(registerRequest.getUserName());
        if (isInDatabase == null) {
            UserEntity userEntity = userMapper.fromDto(registerRequest);
            userRepository.save(userEntity);
        } else {
            log.error("Username " + isInDatabase.getUsername() + " already exists.");
        }
    }

    public void login(LoginRequest loginRequest) {

    }
}


