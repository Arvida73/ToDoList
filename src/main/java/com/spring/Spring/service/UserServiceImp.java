package com.spring.Spring.service;

import com.spring.Spring.dao.TaskEntity;
import com.spring.Spring.dao.UserEntity;
import com.spring.Spring.repository.UserRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImp extends UserService {


    public UserServiceImp(UserRepository userRepository) {
        super(userRepository);
    }
    }

