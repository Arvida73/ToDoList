package com.spring.Spring.service;

import com.spring.Spring.dao.TaskListEntity;
import com.spring.Spring.dto.TaskListRequest;
import com.spring.Spring.mapper.TaskListMapper;
import com.spring.Spring.repository.TaskListInterface;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class TaskListService {


    private final TaskListInterface taskListInterface;
    private final TaskListMapper taskListMapper;

    public void addTaskList(TaskListRequest taskListRequest){
        taskListInterface.save(taskListMapper.fromDto(taskListRequest));
    }

    public void deleteTaskList(Long listId){
        taskListInterface.deleteById(listId);
    }

    public void updateTaskList(Long listId, TaskListEntity newList) {
         TaskListEntity oldlist = taskListInterface.findById(listId).orElseThrow(() -> new IllegalArgumentException());;
        oldlist.setName(newList.getName());
        oldlist.setList(newList.getList());
        oldlist.setUserOwner(newList.getUserOwner());
        oldlist.setUsersWithAccess(newList.getUsersWithAccess());
        taskListInterface.save(oldlist);
    }

}
