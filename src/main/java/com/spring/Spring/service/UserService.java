package com.spring.Spring.service;

import com.spring.Spring.dao.UserEntity;
import com.spring.Spring.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserDetailsService {
    @Autowired
    private final UserRepository userRepository;

    public void register(UserEntity userAccount){
        UserEntity userEntity = new UserEntity();
        userEntity.setUserName(userAccount.getUsername());
        userEntity.setUserPassword(userAccount.getUserPassword());
        userEntity.setRole(userAccount.getRole());
    }
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        return  userRepository.findByUserName(userName);
    }


}
