package com.spring.Spring.service;

import com.spring.Spring.dao.TaskEntity;
import com.spring.Spring.dto.TaskRequest;
import com.spring.Spring.dto.TaskResponse;
import com.spring.Spring.mapper.TaskMapper;
import com.spring.Spring.repository.TaskInterface;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TaskService {

    private TaskMapper taskMapper;
    private  TaskInterface taskInterface;

    public TaskService(TaskMapper taskMapper, TaskInterface taskInterface) {
        this.taskMapper = taskMapper;
        this.taskInterface = taskInterface;
    }

    public TaskResponse getAllTasksByUser(Long taskId){

        TaskEntity taskEntity=taskInterface.findById(taskId)
                .orElseThrow();

        return taskMapper.fromDAO(taskEntity);
    }

    @Transactional
    public void addTask(TaskRequest taskRequest){
        TaskEntity taskEntity= taskMapper.fromDto(taskRequest);
        taskInterface.save(taskEntity);
    }

    public void deleteTask(Long id) {
        taskInterface.deleteById(id);
    }

//    public void updateTask(Long id, TaskEntity taskEntity) {
//        TaskEntity updatedTaskEntity=taskInterface.findById(id).orElseThrow(()->new IllegalArgumentException());
//        updatedTaskEntity.setTaskName(taskEntity.getTaskName());
//        updatedTaskEntity.setDescription(taskEntity.getDescription());
//        updatedTaskEntity.setComments(taskEntity.getComments());
//        updatedTaskEntity.setStatus(taskEntity.getStatus());
//        updatedTaskEntity.setTaskType(taskEntity.getTaskType());
//        updatedTaskEntity.setDateOfCreation(taskEntity.getDateOfCreation());
//        updatedTaskEntity.setDateOfEdition(taskEntity.getDateOfEdition());
//        updatedTaskEntity.setDateOfDeadline(taskEntity.getDateOfDeadline());
//        updatedTaskEntity.setPriority(taskEntity.getPriority());
//        updatedTaskEntity.setComments(taskEntity.getComments());
//
//        taskInterface.save(updatedTaskEntity);
//
//    }


}
