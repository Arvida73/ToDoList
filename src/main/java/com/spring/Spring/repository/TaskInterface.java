package com.spring.Spring.repository;

import com.spring.Spring.dao.TaskEntity;
import com.spring.Spring.dao.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskInterface extends JpaRepository<TaskEntity, Long> {

//    List<TaskEntity> findAllByTaskOwner(UserEntity userEntity);


}
