package com.spring.Spring.repository;

import com.spring.Spring.dao.TaskEntity;
import com.spring.Spring.dao.TaskListEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskListInterface extends JpaRepository<TaskListEntity, Long> {

}
