package com.spring.Spring.dto;

import lombok.Builder;
import lombok.Getter;


@Getter
@Builder
public class TaskListRequest {

    private String name;
    private Long ownerId;


}
