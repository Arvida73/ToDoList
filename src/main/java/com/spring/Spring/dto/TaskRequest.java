package com.spring.Spring.dto;

import lombok.Builder;
import lombok.Getter;

import java.sql.Timestamp;

@Getter
@Builder
public class TaskRequest {

    private Long ownerID;
    private Long listID;
    private String taskName;
    private String description;
    private String status;
    private String taskType;
    private String priority;



}
