package com.spring.Spring.dto;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class RegisterRequest {

    private String userName;
    private String userPassword;
}
