package com.spring.Spring.controller;

import com.spring.Spring.dao.TaskEntity;
import com.spring.Spring.dao.UserEntity;
import com.spring.Spring.dto.TaskRequest;
import com.spring.Spring.dto.TaskResponse;
import com.spring.Spring.service.TaskService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.awt.*;
import java.util.List;

@RestController
@RequestMapping
public class TaskController {

    private final TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @PostMapping(path = "/tasks", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void addTask(@RequestBody TaskRequest taskRequest){
        taskService.addTask(taskRequest);
    }

    //usuwanie taska
    @DeleteMapping("/tasks/{taskid}")
    public void deleteTask(@PathVariable("taskid") Long id){taskService.deleteTask(id);}
    // edycja taska

//    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE,path = "/tasks/{taskid}")
//    public void updateTaskById(@PathVariable("taskid") Long id,@RequestBody TaskEntity taskEntity){
//        taskService.updateTask(id, taskEntity );}
//

//    @GetMapping("/tasks/{userid}")
//    public List<TaskResponse> listOfTasks(@PathVariable("userid")Long userId){
//        return taskService.getAllTasksByUser(userId);
//    }
//
    @GetMapping("/task/{taskId}")
    public TaskResponse getTaskInfo(@PathVariable("taskId")Long taskId){
        return taskService.getAllTasksByUser(taskId);
    }


}
