package com.spring.Spring.controller;

import com.spring.Spring.dao.TaskEntity;
import com.spring.Spring.dao.TaskListEntity;
import com.spring.Spring.dao.UserEntity;
import com.spring.Spring.dto.TaskListRequest;
import com.spring.Spring.service.TaskListService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TaskListController {
    private final TaskListService taskListService;

    public TaskListController(TaskListService taskListService) {
        this.taskListService = taskListService;
    }

    @PostMapping(path = "/ToDoLists", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void addToDoList(@RequestBody TaskListRequest taskListRequest){
        taskListService.addTaskList(taskListRequest);
    }

    @DeleteMapping("/ToDoLists/{listid}")
    public void deleteToDoList(@PathVariable ("listid") Long listId){
        taskListService.deleteTaskList(listId);
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, path ="/ToDoLists/{listid}")
    public void updateToDoListByListId(@PathVariable ("listid") Long listId, @RequestBody TaskListEntity newList){
        taskListService.updateTaskList(listId, newList);
    }



}
