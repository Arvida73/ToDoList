package com.spring.Spring.controller;

import com.spring.Spring.dao.TaskListEntity;
import com.spring.Spring.dao.UserEntity;
import com.spring.Spring.dto.RegisterRequest;
import com.spring.Spring.service.AuthenticationService;
import com.spring.Spring.service.TaskListService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping("/app/")
@AllArgsConstructor
public class UserController {
    private final TaskListService taskListService;
    private final AuthenticationService authenticationService;

    @PostMapping(path = "/signup", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> signup(@RequestBody RegisterRequest registerRequest) {
        authenticationService.signup(registerRequest);
        return new ResponseEntity<>("User Registration Successful", HttpStatus.CREATED);
    }

    @GetMapping("/test1")
    public String test1() {
        return "<h1>Hello</h1>";
    }
}



