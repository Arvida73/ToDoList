package com.spring.Spring.mapper;

import com.spring.Spring.dao.TaskListEntity;
import com.spring.Spring.dao.UserEntity;
import com.spring.Spring.dto.TaskListRequest;
import com.spring.Spring.dto.TaskRequest;
import com.spring.Spring.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class TaskListMapper {

    private final UserRepository userRepository;

    public TaskListEntity fromDto(TaskListRequest taskListRequest){

        UserEntity userOwner=userRepository.findById(taskListRequest.getOwnerId())
                .orElseThrow();

        TaskListEntity taskListEntity=new TaskListEntity();
        taskListEntity.setName(taskListRequest.getName());
        taskListEntity.setUserOwner(userOwner);

        return taskListEntity;

    }
}
