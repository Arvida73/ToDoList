package com.spring.Spring.mapper;

import com.spring.Spring.dao.UserEntity;
import com.spring.Spring.dto.RegisterRequest;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class UserMapper {

    private final PasswordEncoder passwordEncoder;

    public UserEntity fromDto(RegisterRequest registerRequest){

        UserEntity userEntity=new UserEntity();
        userEntity.setUserName(registerRequest.getUserName());
        userEntity.setUserPassword(passwordEncoder.encode(registerRequest.getUserPassword()));
        userEntity.setIsActive(true);
        return userEntity;
    }
}
