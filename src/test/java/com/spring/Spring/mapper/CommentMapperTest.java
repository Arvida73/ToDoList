package com.spring.Spring.mapper;

import com.spring.Spring.dao.Comment;
import com.spring.Spring.dto.CommentRequest;
import com.spring.Spring.dto.CommentResponse;
import com.spring.Spring.repository.CommentRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

class CommentMapperTest {

    private CommentMapper commentMapperUnderTest;

    @BeforeEach
    void setUp() {
        commentMapperUnderTest = new CommentMapper();
        commentMapperUnderTest.repository = mock(CommentRepository.class);
    }

    @Test
    void testFromDTO() {

        final CommentRequest request = CommentRequest.builder().build();
        final Comment expectedResult = new Comment();
        expectedResult.setId(0L);
        expectedResult.setCommentOwner(0L);
//        expectedResult.setTaskEntity(0L);
        expectedResult.setDateOfCreation(new Timestamp(0L));
        expectedResult.setDateOfEdition(new Timestamp(0L));
        expectedResult.setCommentContent("commentContent");


        final Comment result = commentMapperUnderTest.fromDTO(request);


//        assertEquals(expectedResult, result);
    }

    @Test
    void testFromDAO() {

        final Comment comment = new Comment();
        comment.setId(0L);
        comment.setCommentOwner(0L);
//        comment.setTaskEntity(0L);
        comment.setDateOfCreation(new Timestamp(0L));
        comment.setDateOfEdition(new Timestamp(0L));
        comment.setCommentContent("commentContent");


        final CommentResponse result = commentMapperUnderTest.fromDAO(comment);

    }
}
