package com.spring.Spring.mapper;

import com.spring.Spring.dao.*;
import com.spring.Spring.dto.TaskRequest;
import com.spring.Spring.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class TaskMapperTest {

    @Mock
    private UserRepository mockUserRepository;

    private TaskMapper taskMapperUnderTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
//        taskMapperUnderTest = new TaskMapper(mockUserRepository);
    }

    @Test
    void testFromDto() {

        final TaskRequest taskRequest = TaskRequest.builder().build();
        final TaskEntity expectedResult = new TaskEntity();
        expectedResult.setId(0L);
        expectedResult.setTaskName("taskName");
        expectedResult.setDescription("description");
        final UserEntity taskOwnerId = new UserEntity();
        taskOwnerId.setId(0L);
        taskOwnerId.setUserName("userName");
        taskOwnerId.setUserPassword("userPassword");
        taskOwnerId.setIsActive(false);
        taskOwnerId.setRole(Role.USER);
        final TaskListEntity taskListEntity = new TaskListEntity();
        taskListEntity.setId(0L);
        taskListEntity.setName("name");
        taskListEntity.setList(List.of(new TaskEntity()));
        taskListEntity.setUserOwner(new UserEntity());
        taskListEntity.setUsersWithAccess(List.of(new UserEntity()));
        taskOwnerId.setListOwner(List.of(taskListEntity));
        expectedResult.setTaskOwnerId(taskOwnerId);
        expectedResult.setStatus(Status.TO_D0);
        expectedResult.setTaskType(TaskType.HOUSE);
        expectedResult.setDateOfCreation(new Timestamp(0L));
        expectedResult.setDateOfEdition(new Timestamp(0L));
        expectedResult.setDateOfDeadline(new Timestamp(0L));
        expectedResult.setPriority(Priority.LOW);


        final UserEntity userEntity1 = new UserEntity();
        userEntity1.setId(0L);
        userEntity1.setUserName("userName");
        userEntity1.setUserPassword("userPassword");
        userEntity1.setIsActive(false);
        userEntity1.setRole(Role.USER);
        final TaskListEntity taskListEntity1 = new TaskListEntity();
        taskListEntity1.setId(0L);
        taskListEntity1.setName("name");
        final TaskEntity taskEntity = new TaskEntity();
        taskEntity.setId(0L);
        taskEntity.setTaskName("taskName");
        taskEntity.setDescription("description");
        taskEntity.setTaskOwnerId(new UserEntity());
        taskEntity.setStatus(Status.TO_D0);
        taskEntity.setTaskType(TaskType.HOUSE);
        taskEntity.setDateOfCreation(new Timestamp(0L));
        taskEntity.setDateOfEdition(new Timestamp(0L));
        taskEntity.setDateOfDeadline(new Timestamp(0L));
        taskEntity.setPriority(Priority.LOW);
        taskListEntity1.setList(List.of(taskEntity));
        taskListEntity1.setUserOwner(new UserEntity());
        taskListEntity1.setUsersWithAccess(List.of(new UserEntity()));
        userEntity1.setListOwner(List.of(taskListEntity1));
        final Optional<UserEntity> userEntity = Optional.of(userEntity1);
        when(mockUserRepository.findById(0L)).thenReturn(userEntity);


//        final TaskEntity result = taskMapperUnderTest.fromDto(taskRequest);


      //  assertEquals(expectedResult, result);
    }

    @Test
    void testAssignUser() {

        final TaskRequest taskRequest = TaskRequest.builder().build();
        final UserEntity expectedResult = new UserEntity();
        expectedResult.setId(0L);
        expectedResult.setUserName("userName");
        expectedResult.setUserPassword("userPassword");
        expectedResult.setIsActive(false);
        expectedResult.setRole(Role.USER);
        final TaskListEntity taskListEntity = new TaskListEntity();
        taskListEntity.setId(0L);
        taskListEntity.setName("name");
        final TaskEntity taskEntity = new TaskEntity();
        taskEntity.setId(0L);
        taskEntity.setTaskName("taskName");
        taskEntity.setDescription("description");
        taskEntity.setTaskOwnerId(new UserEntity());
        taskEntity.setStatus(Status.TO_D0);
        taskEntity.setTaskType(TaskType.HOUSE);
        taskEntity.setDateOfCreation(new Timestamp(0L));
        taskEntity.setDateOfEdition(new Timestamp(0L));
        taskEntity.setDateOfDeadline(new Timestamp(0L));
        taskEntity.setPriority(Priority.LOW);
        taskListEntity.setList(List.of(taskEntity));
        taskListEntity.setUserOwner(new UserEntity());
        taskListEntity.setUsersWithAccess(List.of(new UserEntity()));
        expectedResult.setListOwner(List.of(taskListEntity));


        final UserEntity userEntity1 = new UserEntity();
        userEntity1.setId(0L);
        userEntity1.setUserName("userName");
        userEntity1.setUserPassword("userPassword");
        userEntity1.setIsActive(false);
        userEntity1.setRole(Role.USER);
        final TaskListEntity taskListEntity1 = new TaskListEntity();
        taskListEntity1.setId(0L);
        taskListEntity1.setName("name");
        final TaskEntity taskEntity1 = new TaskEntity();
        taskEntity1.setId(0L);
        taskEntity1.setTaskName("taskName");
        taskEntity1.setDescription("description");
        taskEntity1.setTaskOwnerId(new UserEntity());
        taskEntity1.setStatus(Status.TO_D0);
        taskEntity1.setTaskType(TaskType.HOUSE);
        taskEntity1.setDateOfCreation(new Timestamp(0L));
        taskEntity1.setDateOfEdition(new Timestamp(0L));
        taskEntity1.setDateOfDeadline(new Timestamp(0L));
        taskEntity1.setPriority(Priority.LOW);
        taskListEntity1.setList(List.of(taskEntity1));
        taskListEntity1.setUserOwner(new UserEntity());
        taskListEntity1.setUsersWithAccess(List.of(new UserEntity()));
        userEntity1.setListOwner(List.of(taskListEntity1));
        final Optional<UserEntity> userEntity = Optional.of(userEntity1);
        when(mockUserRepository.findById(0L)).thenReturn(userEntity);


//        final UserEntity result = taskMapperUnderTest.assignUser(taskRequest);


      //  assertEquals(expectedResult, result);
    }

    @Test
    void testAssignPriority() {

        final TaskRequest taskRequest = TaskRequest.builder().build();


        final Priority result = taskMapperUnderTest.assignPriority(taskRequest);


//       assertEquals(Priority.LOW, result);
    }

    @Test
    void testAssignStatus() {

        final TaskRequest taskRequest = TaskRequest.builder().build();


        final Status result = taskMapperUnderTest.assignStatus(taskRequest);


//        assertEquals(Status.TO_D0, result);
    }

    @Test
    void testAssignTaskType() {

        final TaskRequest taskRequest = TaskRequest.builder().build();


        final TaskType result = taskMapperUnderTest.assignTaskType(taskRequest);


//        assertEquals(TaskType.HOUSE, result);
    }
}
