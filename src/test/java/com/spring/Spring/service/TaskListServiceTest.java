package com.spring.Spring.service;

import com.spring.Spring.dao.*;
import com.spring.Spring.repository.TaskListInterface;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class TaskListServiceTest {

    @Mock
    private TaskListInterface mockTaskListInterface;

    private TaskListService taskListServiceUnderTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
//        taskListServiceUnderTest = new TaskListService(mockTaskListInterface);
    }

    @Test
    void testAddTaskList() {

        final TaskListEntity taskEntityList = new TaskListEntity();
        taskEntityList.setId(0L);
        taskEntityList.setName("name");
        final TaskEntity taskEntity = new TaskEntity();
        taskEntity.setId(0L);
        taskEntity.setTaskName("taskName");
        taskEntity.setDescription("description");
        final UserEntity taskOwnerId = new UserEntity();
        taskOwnerId.setId(0L);
        taskOwnerId.setUserName("userName");
        taskOwnerId.setUserPassword("userPassword");
        taskOwnerId.setIsActive(false);
        taskOwnerId.setRole(Role.USER);
        taskOwnerId.setListOwner(List.of(new TaskListEntity()));
        taskEntity.setTaskOwnerId(taskOwnerId);
        taskEntity.setStatus(Status.TO_D0);
        taskEntity.setTaskType(TaskType.HOUSE);
        taskEntity.setDateOfCreation(new Timestamp(0L));
        taskEntity.setDateOfEdition(new Timestamp(0L));
        taskEntity.setDateOfDeadline(new Timestamp(0L));
        taskEntity.setPriority(Priority.LOW);
        taskEntityList.setList(List.of(taskEntity));
        final UserEntity userOwner = new UserEntity();
        userOwner.setId(0L);
        userOwner.setUserName("userName");
        userOwner.setUserPassword("userPassword");
        userOwner.setIsActive(false);
        userOwner.setRole(Role.USER);
        userOwner.setListOwner(List.of(new TaskListEntity()));
        taskEntityList.setUserOwner(userOwner);
        final UserEntity userEntity = new UserEntity();
        userEntity.setId(0L);
        userEntity.setUserName("userName");
        userEntity.setUserPassword("userPassword");
        userEntity.setIsActive(false);
        userEntity.setRole(Role.USER);
        userEntity.setListOwner(List.of(new TaskListEntity()));
        taskEntityList.setUsersWithAccess(List.of(userEntity));


        final TaskListEntity taskListEntity = new TaskListEntity();
        taskListEntity.setId(0L);
        taskListEntity.setName("name");
        final TaskEntity taskEntity1 = new TaskEntity();
        taskEntity1.setId(0L);
        taskEntity1.setTaskName("taskName");
        taskEntity1.setDescription("description");
        final UserEntity taskOwnerId1 = new UserEntity();
        taskOwnerId1.setId(0L);
        taskOwnerId1.setUserName("userName");
        taskOwnerId1.setUserPassword("userPassword");
        taskOwnerId1.setIsActive(false);
        taskOwnerId1.setRole(Role.USER);
        taskOwnerId1.setListOwner(List.of(new TaskListEntity()));
        taskEntity1.setTaskOwnerId(taskOwnerId1);
        taskEntity1.setStatus(Status.TO_D0);
        taskEntity1.setTaskType(TaskType.HOUSE);
        taskEntity1.setDateOfCreation(new Timestamp(0L));
        taskEntity1.setDateOfEdition(new Timestamp(0L));
        taskEntity1.setDateOfDeadline(new Timestamp(0L));
        taskEntity1.setPriority(Priority.LOW);
        taskListEntity.setList(List.of(taskEntity1));
        final UserEntity userOwner1 = new UserEntity();
        userOwner1.setId(0L);
        userOwner1.setUserName("userName");
        userOwner1.setUserPassword("userPassword");
        userOwner1.setIsActive(false);
        userOwner1.setRole(Role.USER);
        userOwner1.setListOwner(List.of(new TaskListEntity()));
        taskListEntity.setUserOwner(userOwner1);
        final UserEntity userEntity1 = new UserEntity();
        userEntity1.setId(0L);
        userEntity1.setUserName("userName");
        userEntity1.setUserPassword("userPassword");
        userEntity1.setIsActive(false);
        userEntity1.setRole(Role.USER);
        userEntity1.setListOwner(List.of(new TaskListEntity()));
        taskListEntity.setUsersWithAccess(List.of(userEntity1));
        when(mockTaskListInterface.save(new TaskListEntity())).thenReturn(taskListEntity);

//
//        taskListServiceUnderTest.addTaskList(taskEntityList);


    }

    @Test
    void testDeleteTaskList() {

        taskListServiceUnderTest.deleteTaskList(0L);


        verify(mockTaskListInterface).deleteById(0L);
    }

    @Test
    void testUpdateTaskList() {

        final TaskListEntity newList = new TaskListEntity();
        newList.setId(0L);
        newList.setName("name");
        final TaskEntity taskEntity = new TaskEntity();
        taskEntity.setId(0L);
        taskEntity.setTaskName("taskName");
        taskEntity.setDescription("description");
        final UserEntity taskOwnerId = new UserEntity();
        taskOwnerId.setId(0L);
        taskOwnerId.setUserName("userName");
        taskOwnerId.setUserPassword("userPassword");
        taskOwnerId.setIsActive(false);
        taskOwnerId.setRole(Role.USER);
        taskOwnerId.setListOwner(List.of(new TaskListEntity()));
        taskEntity.setTaskOwnerId(taskOwnerId);
        taskEntity.setStatus(Status.TO_D0);
        taskEntity.setTaskType(TaskType.HOUSE);
        taskEntity.setDateOfCreation(new Timestamp(0L));
        taskEntity.setDateOfEdition(new Timestamp(0L));
        taskEntity.setDateOfDeadline(new Timestamp(0L));
        taskEntity.setPriority(Priority.LOW);
        newList.setList(List.of(taskEntity));
        final UserEntity userOwner = new UserEntity();
        userOwner.setId(0L);
        userOwner.setUserName("userName");
        userOwner.setUserPassword("userPassword");
        userOwner.setIsActive(false);
        userOwner.setRole(Role.USER);
        userOwner.setListOwner(List.of(new TaskListEntity()));
        newList.setUserOwner(userOwner);
        final UserEntity userEntity = new UserEntity();
        userEntity.setId(0L);
        userEntity.setUserName("userName");
        userEntity.setUserPassword("userPassword");
        userEntity.setIsActive(false);
        userEntity.setRole(Role.USER);
        userEntity.setListOwner(List.of(new TaskListEntity()));
        newList.setUsersWithAccess(List.of(userEntity));


        final TaskListEntity taskListEntity1 = new TaskListEntity();
        taskListEntity1.setId(0L);
        taskListEntity1.setName("name");
        final TaskEntity taskEntity1 = new TaskEntity();
        taskEntity1.setId(0L);
        taskEntity1.setTaskName("taskName");
        taskEntity1.setDescription("description");
        final UserEntity taskOwnerId1 = new UserEntity();
        taskOwnerId1.setId(0L);
        taskOwnerId1.setUserName("userName");
        taskOwnerId1.setUserPassword("userPassword");
        taskOwnerId1.setIsActive(false);
        taskOwnerId1.setRole(Role.USER);
        taskOwnerId1.setListOwner(List.of(new TaskListEntity()));
        taskEntity1.setTaskOwnerId(taskOwnerId1);
        taskEntity1.setStatus(Status.TO_D0);
        taskEntity1.setTaskType(TaskType.HOUSE);
        taskEntity1.setDateOfCreation(new Timestamp(0L));
        taskEntity1.setDateOfEdition(new Timestamp(0L));
        taskEntity1.setDateOfDeadline(new Timestamp(0L));
        taskEntity1.setPriority(Priority.LOW);
        taskListEntity1.setList(List.of(taskEntity1));
        final UserEntity userOwner1 = new UserEntity();
        userOwner1.setId(0L);
        userOwner1.setUserName("userName");
        userOwner1.setUserPassword("userPassword");
        userOwner1.setIsActive(false);
        userOwner1.setRole(Role.USER);
        userOwner1.setListOwner(List.of(new TaskListEntity()));
        taskListEntity1.setUserOwner(userOwner1);
        final UserEntity userEntity1 = new UserEntity();
        userEntity1.setId(0L);
        userEntity1.setUserName("userName");
        userEntity1.setUserPassword("userPassword");
        userEntity1.setIsActive(false);
        userEntity1.setRole(Role.USER);
        userEntity1.setListOwner(List.of(new TaskListEntity()));
        taskListEntity1.setUsersWithAccess(List.of(userEntity1));
        final Optional<TaskListEntity> taskListEntity = Optional.of(taskListEntity1);
        when(mockTaskListInterface.findById(0L)).thenReturn(taskListEntity);


        final TaskListEntity taskListEntity2 = new TaskListEntity();
        taskListEntity2.setId(0L);
        taskListEntity2.setName("name");
        final TaskEntity taskEntity2 = new TaskEntity();
        taskEntity2.setId(0L);
        taskEntity2.setTaskName("taskName");
        taskEntity2.setDescription("description");
        final UserEntity taskOwnerId2 = new UserEntity();
        taskOwnerId2.setId(0L);
        taskOwnerId2.setUserName("userName");
        taskOwnerId2.setUserPassword("userPassword");
        taskOwnerId2.setIsActive(false);
        taskOwnerId2.setRole(Role.USER);
        taskOwnerId2.setListOwner(List.of(new TaskListEntity()));
        taskEntity2.setTaskOwnerId(taskOwnerId2);
        taskEntity2.setStatus(Status.TO_D0);
        taskEntity2.setTaskType(TaskType.HOUSE);
        taskEntity2.setDateOfCreation(new Timestamp(0L));
        taskEntity2.setDateOfEdition(new Timestamp(0L));
        taskEntity2.setDateOfDeadline(new Timestamp(0L));
        taskEntity2.setPriority(Priority.LOW);
        taskListEntity2.setList(List.of(taskEntity2));
        final UserEntity userOwner2 = new UserEntity();
        userOwner2.setId(0L);
        userOwner2.setUserName("userName");
        userOwner2.setUserPassword("userPassword");
        userOwner2.setIsActive(false);
        userOwner2.setRole(Role.USER);
        userOwner2.setListOwner(List.of(new TaskListEntity()));
        taskListEntity2.setUserOwner(userOwner2);
        final UserEntity userEntity2 = new UserEntity();
        userEntity2.setId(0L);
        userEntity2.setUserName("userName");
        userEntity2.setUserPassword("userPassword");
        userEntity2.setIsActive(false);
        userEntity2.setRole(Role.USER);
        userEntity2.setListOwner(List.of(new TaskListEntity()));
        taskListEntity2.setUsersWithAccess(List.of(userEntity2));
        when(mockTaskListInterface.save(new TaskListEntity())).thenReturn(taskListEntity2);


        taskListServiceUnderTest.updateTaskList(0L, newList);


    }
}
