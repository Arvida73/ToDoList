package com.spring.Spring.service;

import com.spring.Spring.dao.*;
import com.spring.Spring.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.sql.Timestamp;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class UserServiceTest {

    @Mock
    private UserRepository mockUserRepository;

    private UserService userServiceUnderTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
        userServiceUnderTest = new UserService(mockUserRepository);
    }

    @Test
    void testRegister() {

        final UserEntity userAccount = new UserEntity();
        userAccount.setId(0L);
        userAccount.setUserName("userName");
        userAccount.setUserPassword("userPassword");
        userAccount.setIsActive(false);
        userAccount.setRole(Role.USER);
        final TaskListEntity taskListEntity = new TaskListEntity();
        taskListEntity.setId(0L);
        taskListEntity.setName("name");
        final TaskEntity taskEntity = new TaskEntity();
        taskEntity.setId(0L);
        taskEntity.setTaskName("taskName");
        taskEntity.setDescription("description");
        taskEntity.setTaskOwnerId(new UserEntity());
        taskEntity.setStatus(Status.TO_D0);
        taskEntity.setTaskType(TaskType.HOUSE);
        taskEntity.setDateOfCreation(new Timestamp(0L));
        taskEntity.setDateOfEdition(new Timestamp(0L));
        taskEntity.setDateOfDeadline(new Timestamp(0L));
        taskEntity.setPriority(Priority.LOW);
        taskListEntity.setList(List.of(taskEntity));
        taskListEntity.setUserOwner(new UserEntity());
        taskListEntity.setUsersWithAccess(List.of(new UserEntity()));
        userAccount.setListOwner(List.of(taskListEntity));


        userServiceUnderTest.register(userAccount);



    }

    @Test
    void testLoadUserByUsername() {

        final UserEntity userEntity = new UserEntity();
        userEntity.setId(0L);
        userEntity.setUserName("userName");
        userEntity.setUserPassword("userPassword");
        userEntity.setIsActive(false);
        userEntity.setRole(Role.USER);
        final TaskListEntity taskListEntity = new TaskListEntity();
        taskListEntity.setId(0L);
        taskListEntity.setName("name");
        final TaskEntity taskEntity = new TaskEntity();
        taskEntity.setId(0L);
        taskEntity.setTaskName("taskName");
        taskEntity.setDescription("description");
        taskEntity.setTaskOwnerId(new UserEntity());
        taskEntity.setStatus(Status.TO_D0);
        taskEntity.setTaskType(TaskType.HOUSE);
        taskEntity.setDateOfCreation(new Timestamp(0L));
        taskEntity.setDateOfEdition(new Timestamp(0L));
        taskEntity.setDateOfDeadline(new Timestamp(0L));
        taskEntity.setPriority(Priority.LOW);
        taskListEntity.setList(List.of(taskEntity));
        taskListEntity.setUserOwner(new UserEntity());
        taskListEntity.setUsersWithAccess(List.of(new UserEntity()));
        userEntity.setListOwner(List.of(taskListEntity));
        when(mockUserRepository.findByUserName("userName")).thenReturn(userEntity);


        final UserDetails result = userServiceUnderTest.loadUserByUsername("userName");


        verify(mockUserRepository).findByUserName("userName");
    }

    @Test
    void testLoadUserByUsername_ThrowsUsernameNotFoundException() {

        final UserEntity userEntity = new UserEntity();
        userEntity.setId(0L);
        userEntity.setUserName("userName");
        userEntity.setUserPassword("userPassword");
        userEntity.setIsActive(false);
        userEntity.setRole(Role.USER);
        final TaskListEntity taskListEntity = new TaskListEntity();
        taskListEntity.setId(0L);
        taskListEntity.setName("name");
        final TaskEntity taskEntity = new TaskEntity();
        taskEntity.setId(0L);
        taskEntity.setTaskName("taskName");
        taskEntity.setDescription("description");
        taskEntity.setTaskOwnerId(new UserEntity());
        taskEntity.setStatus(Status.TO_D0);
        taskEntity.setTaskType(TaskType.HOUSE);
        taskEntity.setDateOfCreation(new Timestamp(0L));
        taskEntity.setDateOfEdition(new Timestamp(0L));
        taskEntity.setDateOfDeadline(new Timestamp(0L));
        taskEntity.setPriority(Priority.LOW);
        taskListEntity.setList(List.of(taskEntity));
        taskListEntity.setUserOwner(new UserEntity());
        taskListEntity.setUsersWithAccess(List.of(new UserEntity()));
        userEntity.setListOwner(List.of(taskListEntity));
        when(mockUserRepository.findByUserName("userName")).thenReturn(userEntity);


        userServiceUnderTest.loadUserByUsername("userName");
    }
}
