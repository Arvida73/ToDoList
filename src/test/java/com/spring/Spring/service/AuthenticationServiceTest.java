package com.spring.Spring.service;

import com.spring.Spring.dao.*;
import com.spring.Spring.dto.LoginRequest;
import com.spring.Spring.dto.RegisterRequest;
import com.spring.Spring.mapper.UserMapper;
import com.spring.Spring.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.sql.Timestamp;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class AuthenticationServiceTest {

    @Mock
    private UserMapper mockUserMapper;
    @Mock
    private UserRepository mockUserRepository;

    private AuthenticationService authenticationServiceUnderTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
        authenticationServiceUnderTest = new AuthenticationService(mockUserMapper, mockUserRepository);
    }

    @Test
    void testSignup() {

        final RegisterRequest registerRequest = RegisterRequest.builder().build();


        final UserEntity userEntity = new UserEntity();
        userEntity.setId(0L);
        userEntity.setUserName("userName");
        userEntity.setUserPassword("userPassword");
        userEntity.setIsActive(false);
        userEntity.setRole(Role.USER);
        final TaskListEntity taskListEntity = new TaskListEntity();
        taskListEntity.setId(0L);
        taskListEntity.setName("name");
        final TaskEntity taskEntity = new TaskEntity();
        taskEntity.setId(0L);
        taskEntity.setTaskName("taskName");
        taskEntity.setDescription("description");
        taskEntity.setTaskOwnerId(new UserEntity());
        taskEntity.setStatus(Status.TO_D0);
        taskEntity.setTaskType(TaskType.HOUSE);
        taskEntity.setDateOfCreation(new Timestamp(0L));
        taskEntity.setDateOfEdition(new Timestamp(0L));
        taskEntity.setDateOfDeadline(new Timestamp(0L));
        taskEntity.setPriority(Priority.LOW);
        taskListEntity.setList(List.of(taskEntity));
        taskListEntity.setUserOwner(new UserEntity());
        taskListEntity.setUsersWithAccess(List.of(new UserEntity()));
        userEntity.setListOwner(List.of(taskListEntity));
        when(mockUserRepository.findByUserName("userName")).thenReturn(userEntity);


        final UserEntity userEntity1 = new UserEntity();
        userEntity1.setId(0L);
        userEntity1.setUserName("userName");
        userEntity1.setUserPassword("userPassword");
        userEntity1.setIsActive(false);
        userEntity1.setRole(Role.USER);
        final TaskListEntity taskListEntity1 = new TaskListEntity();
        taskListEntity1.setId(0L);
        taskListEntity1.setName("name");
        final TaskEntity taskEntity1 = new TaskEntity();
        taskEntity1.setId(0L);
        taskEntity1.setTaskName("taskName");
        taskEntity1.setDescription("description");
        taskEntity1.setTaskOwnerId(new UserEntity());
        taskEntity1.setStatus(Status.TO_D0);
        taskEntity1.setTaskType(TaskType.HOUSE);
        taskEntity1.setDateOfCreation(new Timestamp(0L));
        taskEntity1.setDateOfEdition(new Timestamp(0L));
        taskEntity1.setDateOfDeadline(new Timestamp(0L));
        taskEntity1.setPriority(Priority.LOW);
        taskListEntity1.setList(List.of(taskEntity1));
        taskListEntity1.setUserOwner(new UserEntity());
        taskListEntity1.setUsersWithAccess(List.of(new UserEntity()));
        userEntity1.setListOwner(List.of(taskListEntity1));
        when(mockUserMapper.fromDto(any(RegisterRequest.class))).thenReturn(userEntity1);


        final UserEntity userEntity2 = new UserEntity();
        userEntity2.setId(0L);
        userEntity2.setUserName("userName");
        userEntity2.setUserPassword("userPassword");
        userEntity2.setIsActive(false);
        userEntity2.setRole(Role.USER);
        final TaskListEntity taskListEntity2 = new TaskListEntity();
        taskListEntity2.setId(0L);
        taskListEntity2.setName("name");
        final TaskEntity taskEntity2 = new TaskEntity();
        taskEntity2.setId(0L);
        taskEntity2.setTaskName("taskName");
        taskEntity2.setDescription("description");
        taskEntity2.setTaskOwnerId(new UserEntity());
        taskEntity2.setStatus(Status.TO_D0);
        taskEntity2.setTaskType(TaskType.HOUSE);
        taskEntity2.setDateOfCreation(new Timestamp(0L));
        taskEntity2.setDateOfEdition(new Timestamp(0L));
        taskEntity2.setDateOfDeadline(new Timestamp(0L));
        taskEntity2.setPriority(Priority.LOW);
        taskListEntity2.setList(List.of(taskEntity2));
        taskListEntity2.setUserOwner(new UserEntity());
        taskListEntity2.setUsersWithAccess(List.of(new UserEntity()));
        userEntity2.setListOwner(List.of(taskListEntity2));
        when(mockUserRepository.save(new UserEntity())).thenReturn(userEntity2);


        authenticationServiceUnderTest.signup(registerRequest);


    }

    @Test
    void testLogin() {

        final LoginRequest loginRequest = LoginRequest.builder().build();


        authenticationServiceUnderTest.login(loginRequest);


    }
}
