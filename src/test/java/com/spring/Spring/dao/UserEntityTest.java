package com.spring.Spring.dao;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class UserEntityTest {

    private UserEntity userEntityUnderTest;

    @BeforeEach
    void setUp() {
        userEntityUnderTest = new UserEntity();
    }

    @Test
    void testGetAuthorities() {

        final Collection<? extends GrantedAuthority> result = userEntityUnderTest.getAuthorities();

    }

    @Test
    void testGetPassword() {

        final String result = userEntityUnderTest.getPassword();


//        assertEquals("null", result);
    }

    @Test
    void testIsAccountNonExpired() {

        final boolean result = userEntityUnderTest.isAccountNonExpired();


        assertTrue(result);
    }

    @Test
    void testIsAccountNonLocked() {

        final boolean result = userEntityUnderTest.isAccountNonLocked();


        assertTrue(result);
    }

    @Test
    void testIsCredentialsNonExpired() {

        final boolean result = userEntityUnderTest.isCredentialsNonExpired();


        assertTrue(result);
    }

    @Test
    void testIsEnabled() {

        final boolean result = userEntityUnderTest.isEnabled();


        assertTrue(result);
    }

    @Test
    void testEquals() {

        final boolean result = userEntityUnderTest.equals("o");


       // assertTrue(result);
    }

    @Test
    void testHashCode() {

        final int result = userEntityUnderTest.hashCode();


       // assertEquals(0, result);
    }

    @Test
    void testToString() {

        final String result = userEntityUnderTest.toString();


     //   assertEquals("result", result);
    }
}
