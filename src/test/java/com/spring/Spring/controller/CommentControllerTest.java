package com.spring.Spring.controller;

import com.spring.Spring.dao.Comment;
import com.spring.Spring.dto.CommentRequest;
import com.spring.Spring.dto.CommentResponse;
import com.spring.Spring.service.CommentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

class CommentControllerTest {

    private CommentController commentControllerUnderTest;

    @BeforeEach
    void setUp() {
//        commentControllerUnderTest = new CommentController();
        commentControllerUnderTest.commentService = mock(CommentService.class);
    }

    @Test
    void testAddComment() {

        final Comment comment = new Comment();
        comment.setId(0L);
        comment.setCommentOwner(0L);
//        comment.setTaskEntity(0L);
        comment.setDateOfCreation(new Timestamp(0L));
        comment.setDateOfEdition(new Timestamp(0L));
        comment.setCommentContent("commentContent");


//        commentControllerUnderTest.addComment(comment, 0L);


        //verify(commentControllerUnderTest.commentService).addCommentToList(new Comment(), 0L);
    }

    @Test
    void testGetAllComments() {

//        final List<CommentResponse> commentResponses = List.of(CommentResponse.builder().build());
//        when(commentControllerUnderTest.commentService.getAll(taskId)).thenReturn(commentResponses);
//
//
//        final List<CommentResponse> result = commentControllerUnderTest.getAllComments();


    }

    @Test
    void testDeleteComment() {

        commentControllerUnderTest.deleteComment(0L);


        verify(commentControllerUnderTest.commentService).deleteComment(0L);
    }

    @Test
    void testGetComment() {

        when(commentControllerUnderTest.commentService.getComment(0L)).thenReturn(CommentResponse.builder().build());


        final CommentResponse result = commentControllerUnderTest.getComment(0L);


    }

    @Test
    void testEditComment() {

        final CommentRequest request = CommentRequest.builder().build();


//        commentControllerUnderTest.editComment(0L, request);
//
//
//        verify(commentControllerUnderTest.commentService).editComment(eq(0L), any(CommentRequest.class));
    }
}
