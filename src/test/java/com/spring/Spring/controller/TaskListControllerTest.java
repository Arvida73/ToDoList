package com.spring.Spring.controller;

import com.spring.Spring.dao.*;
import com.spring.Spring.service.TaskListService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.sql.Timestamp;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

class TaskListControllerTest {

    @Mock
    private TaskListService mockTaskListService;

    private TaskListController taskListControllerUnderTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
        taskListControllerUnderTest = new TaskListController(mockTaskListService);
    }

    @Test
    void testAddToDoList() {

        final TaskListEntity taskList = new TaskListEntity();
        taskList.setId(0L);
        taskList.setName("name");
        final TaskEntity taskEntity = new TaskEntity();
        taskEntity.setId(0L);
        taskEntity.setTaskName("nazwa tasku");
        taskEntity.setDescription("opis");
        final UserEntity taskOwnerId = new UserEntity();
        taskOwnerId.setId(0L);
        taskOwnerId.setUserName("uzytkownik");
        taskOwnerId.setUserPassword("haslo");
        taskOwnerId.setIsActive(false);
        taskOwnerId.setRole(Role.USER);
        taskOwnerId.setListOwner(List.of(new TaskListEntity()));
        taskEntity.setTaskOwnerId(taskOwnerId);
        taskEntity.setStatus(Status.TO_D0);
        taskEntity.setTaskType(TaskType.HOUSE);
        taskEntity.setDateOfCreation(new Timestamp(0L));
        taskEntity.setDateOfEdition(new Timestamp(0L));
        taskEntity.setDateOfDeadline(new Timestamp(0L));
        taskEntity.setPriority(Priority.LOW);
        taskList.setList(List.of(taskEntity));
        final UserEntity userOwner = new UserEntity();
        userOwner.setId(0L);
        userOwner.setUserName("uzytkownik");
        userOwner.setUserPassword("haslo");
        userOwner.setIsActive(false);
        userOwner.setRole(Role.USER);
        userOwner.setListOwner(List.of(new TaskListEntity()));
        taskList.setUserOwner(userOwner);
        final UserEntity userEntity = new UserEntity();
        userEntity.setId(0L);
        userEntity.setUserName("uzytkownik");
        userEntity.setUserPassword("haslo");
        userEntity.setIsActive(false);
        userEntity.setRole(Role.USER);
        userEntity.setListOwner(List.of(new TaskListEntity()));
        taskList.setUsersWithAccess(List.of(userEntity));

//        taskListControllerUnderTest.addToDoList(taskList);


        //(mockTaskListService).addTaskList(new TaskListEntity());
    }

    @Test
    void testDeleteToDoList() {

        taskListControllerUnderTest.deleteToDoList(0L);

        verify(mockTaskListService).deleteTaskList(0L);
    }

    @Test
    void testUpdateToDoListByListId() {

        final TaskListEntity newList = new TaskListEntity();
        newList.setId(0L);
        newList.setName("name");
        final TaskEntity taskEntity = new TaskEntity();
        taskEntity.setId(0L);
        taskEntity.setTaskName("nazwa tasku");
        taskEntity.setDescription("opis");
        final UserEntity taskOwnerId = new UserEntity();
        taskOwnerId.setId(0L);
        taskOwnerId.setUserName("uzytkownik");
        taskOwnerId.setUserPassword("haslo");
        taskOwnerId.setIsActive(false);
        taskOwnerId.setRole(Role.USER);
        taskOwnerId.setListOwner(List.of(new TaskListEntity()));
        taskEntity.setTaskOwnerId(taskOwnerId);
        taskEntity.setStatus(Status.TO_D0);
        taskEntity.setTaskType(TaskType.HOUSE);
        taskEntity.setDateOfCreation(new Timestamp(0L));
        taskEntity.setDateOfEdition(new Timestamp(0L));
        taskEntity.setDateOfDeadline(new Timestamp(0L));
        taskEntity.setPriority(Priority.LOW);
        newList.setList(List.of(taskEntity));
        final UserEntity userOwner = new UserEntity();
        userOwner.setId(0L);
        userOwner.setUserName("uzytkownik");
        userOwner.setUserPassword("haslo");
        userOwner.setIsActive(false);
        userOwner.setRole(Role.USER);
        userOwner.setListOwner(List.of(new TaskListEntity()));
        newList.setUserOwner(userOwner);
        final UserEntity userEntity = new UserEntity();
        userEntity.setId(0L);
        userEntity.setUserName("uzytkownik");
        userEntity.setUserPassword("haslo");
        userEntity.setIsActive(false);
        userEntity.setRole(Role.USER);
        userEntity.setListOwner(List.of(new TaskListEntity()));
        newList.setUsersWithAccess(List.of(userEntity));

        taskListControllerUnderTest.updateToDoListByListId(0L, newList);


       // (mockTaskListService).updateTaskList(0L, new TaskListEntity());
    }
}
