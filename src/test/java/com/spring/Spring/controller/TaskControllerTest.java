package com.spring.Spring.controller;

import com.spring.Spring.dao.*;
import com.spring.Spring.service.TaskService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.sql.Timestamp;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class TaskControllerTest {

    @Mock
    private TaskService mockTaskService;

    private TaskController taskControllerUnderTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
        taskControllerUnderTest = new TaskController(mockTaskService);
    }

    @Test
    void testAddTask() {

        final TaskEntity taskEntity = new TaskEntity();
        taskEntity.setId(0L);
        taskEntity.setTaskName("taskName");
        taskEntity.setDescription("description");
        final UserEntity taskOwnerId = new UserEntity();
        taskOwnerId.setId(0L);
        taskOwnerId.setUserName("userName");
        taskOwnerId.setUserPassword("userPassword");
        taskOwnerId.setIsActive(false);
        taskOwnerId.setRole(Role.USER);
        final TaskListEntity taskListEntity = new TaskListEntity();
        taskListEntity.setId(0L);
        taskListEntity.setName("name");
        taskListEntity.setList(List.of(new TaskEntity()));
        taskListEntity.setUserOwner(new UserEntity());
        taskListEntity.setUsersWithAccess(List.of(new UserEntity()));
        taskOwnerId.setListOwner(List.of(taskListEntity));
        taskEntity.setTaskOwnerId(taskOwnerId);
        taskEntity.setStatus(Status.TO_D0);
        taskEntity.setTaskType(TaskType.HOUSE);
        taskEntity.setDateOfCreation(new Timestamp(0L));
        taskEntity.setDateOfEdition(new Timestamp(0L));
        taskEntity.setDateOfDeadline(new Timestamp(0L));
        taskEntity.setPriority(Priority.LOW);


//        taskControllerUnderTest.addTask(taskEntity);


//        (mockTaskService).addTask(new TaskEntity());
    }

    @Test
    void testDeleteTask() {

        taskControllerUnderTest.deleteTask(0L);


        verify(mockTaskService).deleteTask(0L);
    }

    @Test
    void testUpdateTaskById() {

        final TaskEntity taskEntity = new TaskEntity();
        taskEntity.setId(0L);
        taskEntity.setTaskName("taskName");
        taskEntity.setDescription("description");
        final UserEntity taskOwnerId = new UserEntity();
        taskOwnerId.setId(0L);
        taskOwnerId.setUserName("userName");
        taskOwnerId.setUserPassword("userPassword");
        taskOwnerId.setIsActive(false);
        taskOwnerId.setRole(Role.USER);
        final TaskListEntity taskListEntity = new TaskListEntity();
        taskListEntity.setId(0L);
        taskListEntity.setName("name");
        taskListEntity.setList(List.of(new TaskEntity()));
        taskListEntity.setUserOwner(new UserEntity());
        taskListEntity.setUsersWithAccess(List.of(new UserEntity()));
        taskOwnerId.setListOwner(List.of(taskListEntity));
        taskEntity.setTaskOwnerId(taskOwnerId);
        taskEntity.setStatus(Status.TO_D0);
        taskEntity.setTaskType(TaskType.HOUSE);
        taskEntity.setDateOfCreation(new Timestamp(0L));
        taskEntity.setDateOfEdition(new Timestamp(0L));
        taskEntity.setDateOfDeadline(new Timestamp(0L));
        taskEntity.setPriority(Priority.LOW);


//        taskControllerUnderTest.updateTaskById(0L, taskEntity);


        //(mockTaskService).updateTask(0L, new TaskEntity());
    }

    @Test
    void testListOfTasks() {

        final TaskEntity taskEntity = new TaskEntity();
        taskEntity.setId(0L);
        taskEntity.setTaskName("taskName");
        taskEntity.setDescription("description");
        final UserEntity taskOwnerId = new UserEntity();
        taskOwnerId.setId(0L);
        taskOwnerId.setUserName("userName");
        taskOwnerId.setUserPassword("userPassword");
        taskOwnerId.setIsActive(false);
        taskOwnerId.setRole(Role.USER);
        final TaskListEntity taskListEntity = new TaskListEntity();
        taskListEntity.setId(0L);
        taskListEntity.setName("name");
        taskListEntity.setList(List.of(new TaskEntity()));
        taskListEntity.setUserOwner(new UserEntity());
        taskListEntity.setUsersWithAccess(List.of(new UserEntity()));
        taskOwnerId.setListOwner(List.of(taskListEntity));
        taskEntity.setTaskOwnerId(taskOwnerId);
        taskEntity.setStatus(Status.TO_D0);
        taskEntity.setTaskType(TaskType.HOUSE);
        taskEntity.setDateOfCreation(new Timestamp(0L));
        taskEntity.setDateOfEdition(new Timestamp(0L));
        taskEntity.setDateOfDeadline(new Timestamp(0L));
        taskEntity.setPriority(Priority.LOW);
        final List<TaskEntity> expectedResult = List.of(taskEntity);


        final TaskEntity taskEntity1 = new TaskEntity();
        taskEntity1.setId(0L);
        taskEntity1.setTaskName("taskName");
        taskEntity1.setDescription("description");
        final UserEntity taskOwnerId1 = new UserEntity();
        taskOwnerId1.setId(0L);
        taskOwnerId1.setUserName("userName");
        taskOwnerId1.setUserPassword("userPassword");
        taskOwnerId1.setIsActive(false);
        taskOwnerId1.setRole(Role.USER);
        final TaskListEntity taskListEntity1 = new TaskListEntity();
        taskListEntity1.setId(0L);
        taskListEntity1.setName("name");
        taskListEntity1.setList(List.of(new TaskEntity()));
        taskListEntity1.setUserOwner(new UserEntity());
        taskListEntity1.setUsersWithAccess(List.of(new UserEntity()));
        taskOwnerId1.setListOwner(List.of(taskListEntity1));
        taskEntity1.setTaskOwnerId(taskOwnerId1);
        taskEntity1.setStatus(Status.TO_D0);
        taskEntity1.setTaskType(TaskType.HOUSE);
        taskEntity1.setDateOfCreation(new Timestamp(0L));
        taskEntity1.setDateOfEdition(new Timestamp(0L));
        taskEntity1.setDateOfDeadline(new Timestamp(0L));
        taskEntity1.setPriority(Priority.LOW);
        final List<TaskEntity> taskEntities = List.of(taskEntity1);
//        when(mockTaskService.getAllTasksByUser(0L)).thenReturn(taskEntities);
//
//
//        final List<TaskEntity> result = taskControllerUnderTest.listOfTasks(0L);

//
//        assertEquals(expectedResult, result);
    }
}
