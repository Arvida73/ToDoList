package com.spring.Spring.controller;

import com.spring.Spring.dto.RegisterRequest;
import com.spring.Spring.service.AuthenticationService;
import com.spring.Spring.service.TaskListService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpSession;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

class UserControllerTest {

    @Mock
    private TaskListService mockTaskListService;
    @Mock
    private AuthenticationService mockAuthenticationService;

    private UserController userControllerUnderTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
        userControllerUnderTest = new UserController(mockTaskListService, mockAuthenticationService);
    }

    @Test
    void testSignup() {

        final RegisterRequest registerRequest = RegisterRequest.builder().build();
        final ResponseEntity<String> expectedResult = new ResponseEntity<>("User Registration Successful", HttpStatus.CREATED);

        final ResponseEntity<String> result = userControllerUnderTest.signup(registerRequest);


        assertEquals(expectedResult, result);
      verify(mockAuthenticationService).signup(any(RegisterRequest.class));
    }

    @Test
    void testTest1() {

        final String result = userControllerUnderTest.test1();


        assertEquals("<h1>Hello</h1>", result);
    }

//    @Test
//    void testLogoutUser() {
//
//
//        final HttpSession session = null;
//
//
//        final String result = userControllerUnderTest.logoutUser(session);
//
//
//        assertEquals("result", result);
    }

